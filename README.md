# Razzle x React - Jogo da Velha

## Quer testar?

Baixe o exemplo [ou clone este projeto](https://github.com/henriquezolini/razzle-react-game.git):

```bash
curl https://codeload.github.com/henriquezolini/razzle-react-game/tar.gz/master
cd razzle-react-game
```

Instale e execute

```bash
npm install
npm start
```

## Idéia

Exemplo básico de um jogo da velha utilizado Razzel e React.
